<?php
require 'vendor/autoload.php';

$def = new Symfony\Component\Dotenv\Dotenv();
$def->loadEnv(__DIR__.'/.env');

#var_dump($_ENV); die();

$nsqlookupd_url = $_ENV['NSQLOOKUPD_HOST'].':'.$_ENV['NSQLOOKUPD_PORT'];
$nsq_lookupd = new NsqLookupd($nsqlookupd_url); //the nsqlookupd http addr
$nsq = new Nsq();

$config = array(
  "topic" => $_ENV['TOPIC'],
  "channel" => $_ENV['CHANNEL'],
  "rdy" => (INT)$_ENV['RDY'],                //optional , default 1
  "connect_num" => (INT)$_ENV['CONNECT_NUM'],        //optional , default 1   
  "retry_delay_time" => (INT)$_ENV['RETRY_DELAY_TIME'],  //optional, default 0 , if run callback failed, after 5000 msec, message will be retried
  "auto_finish" => $_ENV['AUTO_FINISH'], //default true

);

$nsq->subscribe($nsq_lookupd, $config, function($msg,$bev){
    $nama_perpustakaan = $_ENV['NAMA_PERPUSTAKAAN'];
    $circ = json_decode($msg->payload);

    # DEBUG NSQ
    #echo "=====\n";
    #echo $msg->payload."\n";
    #echo $msg->attempts."\n";
    #echo $msg->messageId."\n";
    #echo $msg->timestamp."\n";
    #$message = 'Tes. Bismillah. no.11';
    #var_dump($circ);

    $message = '*'.strtoupper($nama_perpustakaan)."*\n";
    $message .= 'No. Angg : '.$circ->circ_data->memberID."\n";
    $message .= 'Nama : '.$circ->circ_data->memberName."\n";
    $message .= 'Jn. Angg : '.$circ->circ_data->memberType."\n";
    $message .= 'Tanggal : '.$circ->circ_data->date."\n";
    $message .= 'ID : '.$msg->messageId."\n";

    if (isset($circ->circ_data->loan)) {
        $message .= "=====================\n";
        $message .= "*PEMINJAMAN*\n";
        $message .= "=====================\n";
        foreach($circ->circ_data->loan as $lk => $lv) {
            $message .= '*'.$lv->itemCode."*\n";
            $message .= '_'.$lv->title."_\n";
            $loanDate = explode('-', $lv->loanDate);
            $message .= 'Tanggal pinjam: '.$loanDate[2].'-'.$loanDate[1].'-'.$loanDate[0]."\n";
            $dueDate = explode('-', $lv->dueDate);
            $message .= 'Batas pinjam: '.$dueDate[2].'-'.$dueDate[1].'-'.$dueDate[0]."\n";
        }
    }

    if (isset($circ->circ_data->return)) {
        $counter = 0;
        $retmessage = "=====================\n";
        $retmessage .= "*PENGEMBALIAN*\n";
        $retmessage .= "=====================\n";
        foreach($circ->circ_data->return as $rk => $rv) {
            $dup = FALSE;
            if (isset($circ->circ_data->extend)) {
                foreach ($circ->circ_data->extend as $_ek => $_ev) {
                    if ($rv->itemCode == $_ev->itemCode) {
                        $dup = TRUE;
                    }
                }
            }
            if (!$dup) {
                $retmessage .= '*'.$rv->itemCode."*\n";
                $retmessage .= '_'.$rv->title."_\n";
                $returnDate = explode('-', $rv->returnDate);
                $retmessage .= 'Tanggal kembali: '.$returnDate[2].'-'.$returnDate[1].'-'.$returnDate[0]."\n";
                if ($rv->overdues) {
                    $retmessage .= 'Denda: '.$rv->overdues."\n";
                }
                $counter++;                  
            }
        }
        if ($counter > 0) {
            $message .= $retmessage;
        }
    }

    if (isset($circ->circ_data->extend)) {
        $message .= "=====================\n";
        $message .= "*PERPANJANGAN*\n";
        $message .= "=====================\n";
        foreach($circ->circ_data->extend as $ek => $ev) {
            $message .= '*'.$ev->itemCode."*\n";
            $message .= '_'.$ev->title."_\n";
            $loanDate = explode('-', $ev->loanDate);
            $message .= 'Tanggal pinjam: '.$loanDate[2].'-'.$loanDate[1].'-'.$loanDate[0]."\n";
            $dueDate = explode('-', $ev->dueDate);
            $message .= 'Batas pinjam: '.$dueDate[2].'-'.$dueDate[1].'-'.$dueDate[0]."\n";
        }
    }
    $message .= "\n_____________________\n".'Harap simpan resi ini';

    $data = array (
        'device_id' => $_ENV['DEVICE_ID'],
        'number' => $circ->member_phone,
        'message' => $message
    );
    $client = new \GuzzleHttp\Client();
    $response = $client->request('POST', 'https://app.whacenter.com/api/send', [
        'form_params' => $data
    ]);

});




